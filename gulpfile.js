// @todo add gulp-plumber so ng-annotate, gulp-sass, etc don't kill the process
var path = require("path");
var fse = require("fs-extra");

var Mustache = require("mustache");
var Server = require("http-server");

var gulp = require("gulp");
var plugins = require("gulp-load-plugins")({
  rename: {
    "gulp-ng-annotate": "annotate",
    "gulp-ng-html2js": "html2js"
  }
});

var options = {
  "path": {
    "output": "public/",
    "js": "js",
    "css": "css",
    "templates": "js"
  },
  "js": {
    "glob": [
      "node_modules/angular/angular.js",
      "node_modules/angular-route/angular-route.js",
      "src/**/*.js",
      "src/app.js"
    ]
  },
  "css": {
    "glob": [
      // "src/style/*.scss",
      "src/style/*.+(css|scss)",
      "src/**/*.+(css|scss)"
    ],
    "sassIncludes": "src/style/includes"
  },
  "templates": {
    "glob": [
      "src/**/*.html",
      "!src/index.html"
    ],
    "moduleName": "templates",
    "rename": function(url) {
      return url.split("/").pop().replace(".html", "");
    }
  },
  "indexTemplate": path.join(__dirname, "src", "index.html"),
  "server": {
    "host": "localhost",
    "port": 8880
  },
  "data": {
    "site": require(path.join(__dirname, "src/data/site.json"))
  }
};

function onError() {
  console.log(arguments);
}

function css() {

  var result = gulp.src(options.css.glob)
    .pipe(plugins.sass({ includePaths: options.css.sassIncludes }))
    .pipe(plugins.concat("main.css"))
    .pipe(gulp.dest(path.join(options.path.output, options.path.css)));

  result.on("error", onError);

  return result;
}

function js() {

  var result = gulp.src(options.js.glob)
    .pipe(plugins.concat("app.js"))
    // .pipe(plugins.uglify())
    .pipe(gulp.dest(path.join(options.path.output, options.path.js)));

  result.on("error", onError);

  return result;
}

function templates() {

  var result = gulp.src(options.templates.glob)
    .pipe(plugins.html2js({
      moduleName: options.templates.moduleName,
      rename: options.templates.rename
    }))
    .pipe(plugins.concat("templates.js"))
    .pipe(plugins.uglify())
    .pipe(gulp.dest(path.join(options.path.output, options.path.js)));

  result.on("error", onError);

  return result;
}

function mustache() {

  // import ENV variables into app.js
  // fse.outputFileSync(path.join(__dirname, "src", "app.js"), options.data.site);

  var render = Mustache.render(fse.readFileSync(path.join(__dirname, "src", "index.html")).toString(), options.data.site);

  fse.outputFileSync(path.join(__dirname, options.path.output, "index.html"), render);

}

function serve() {

  var server = Server.createServer({ root: path.join(process.cwd(), options.path.output) });
  server.listen(options.server.port, options.server.host);

  console.log([
                "Server listening on ",
                options.server.host,
                ":",
                options.server.port].join("")
                );
}

function moveStaticFiles() {
//   gulp.src([
//       "src/index.html",
//       "src/.htaccess"
//     ])
}

function watch() {

  gulp.watch([path.join(__dirname, "src", "index.html"), path.join(__dirname, "src/data/site.json")], ["mustache"]);

  gulp.watch(options.js.glob, ["js"]);
  gulp.watch(options.css.glob, ["css"]);
  gulp.watch(options.templates.glob, ["templates"]);

  // may fail
  // "The recursive option is only supported on OS X and Windows."
  // https://nodejs.org/api/fs.html#fs_fs_watch_filename_options_listener
  try {
    fse.watch(path.join(__dirname, "src"), { recursive: true }, function() {
      build();
    });
  } catch(e) {
    throw e;
  }

}


function build() {
  fse.emptyDirSync(path.join(__dirname, "public"));
  js();
  css();
  templates();
  mustache();
}

gulp.task("test", function() {
  console.log(options.data.site.ENV);
});

gulp.task("js", js);
gulp.task("css", css);
gulp.task("templates", templates);
gulp.task("mustache", mustache);

gulp.task("serve", serve);
gulp.task("watch", watch);
gulp.task("build", build);
gulp.task("default", ["build", "serve", "watch"]);
