angular.module("app", ["ngRoute", "templates", "components"])
  .constant("ENV", {
    "NASA_API_KEY": "DEMO_KEY"
  })
  .config(function($routeProvider) {

    $routeProvider.when("/", {
      controller: "SiteController",
      templateUrl: "site"
    })
    .otherwise({ redirectTo: "/" });

  }).run(function($rootScope) {
    console.log("app::run()");
  });
