angular.module("site-controller", [])
  .controller("SiteController", function SiteController($scope, nasaImageService) {

    function formatDate(date) {

      function addLeadingZero(input) {
        if (input.toString().length < 2) {
          return "0".concat(input.toString());
        } else {
          return input.toString();
        }
      }

      return [
        date.year,
        addLeadingZero(date.month),
        addLeadingZero(date.day)
        ].join("-");
    }

    var today = new Date();
    $scope.date = {
      "month": today.getUTCMonth() + 1,
      "day": today.getUTCDate(),
      "year": today.getUTCFullYear()
    };

    $scope.formatDate = formatDate($scope.date);

    $scope.updateImage = function() {
      $scope.formatDate = formatDate($scope.date);
    };

  });
