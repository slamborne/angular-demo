angular.module("nasa-image-directive", [])
  .directive("nasaImageDirective", function NasaImageDirective(nasaImageService) {

    return {
      restrict: "E",
      scope: true,
      templateUrl: "nasa-image-directive",
      link: function(scope, element, attr) {

        function getImage(date) {
          nasaImageService.fetch(date)
            .error(function(result) {
              if (result.error && result.error.code) {
                throw new Error(result.error.code);
              }
            })
            .success(function(result) {
              scope.data = result;
            });
        }

        scope.data = {};

        scope.$watch("formatDate", function(newValue, oldValue) {
          if (newValue !== oldValue) {
            getImage(newValue);
          }
        });

        getImage();

      }
    };
  });
