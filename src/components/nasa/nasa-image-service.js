angular.module("nasa-image-service", [])
  .service("nasaImageService", function NasaImageService($http, ENV) {

    this.fetch = function(date) {
      date = date || "2014-06-01";

      return $http.get([
                       "https://api.nasa.gov/planetary/apod",
                       "?api_key=",
                       ENV.NASA_API_KEY,
                       "&date=",
                       date,
                       "&hd=true"
                       ].join(""));
    };

  });
